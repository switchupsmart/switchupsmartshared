#define BLYNK_TEMPLATE_ID "TMPL2oYPk_sOU"
#define BLYNK_TEMPLATE_NAME "smart home"
#define BLYNK_AUTH_TOKEN "acLiX5I5qNn6nwOdp183wjd7tAEmny7p"
#define BLYNK_PRINT Serial

#include <Servo.h>
#include <ESP8266WiFi.h> 
#include <BlynkSimpleEsp8266.h> 
#include <Wire.h> 
#include <AHT20.h>
// Declare Blynk token 

char auth[] = " acLiX5I5qNn6nwOdp183wjd7tAEmny7p "; // auth token of the project 
// wifi connected
char ssid[] = "Minh";
char pass[] = "123456789";

AHT20 aht20; 
Servo s1; 
Servo s2; 
int button = 2;
int button2 = 14;
int temp = 0; 
int temp2 = 0;
int led = 15; 
int motion = 13;
int position_target =180; 
int position_last_set =0; 
int button_pin = V0; 
int button2_pin = V1; 
int buttonState =0; 
void setup() {
 Blynk.begin(auth, ssid, pass);
  // put your setup code here, to run once:
 Serial.begin(115200);
 Serial.println("Welcome to our team SwitchUpSmart");
 Serial.println("Humidity:"); 
 Wire.begin(); 
 if (aht20.begin()==false){
  Serial.println("AHT20 not detected. Someone is hacking");
 }
 Serial.println("AHT20 acknowledged.");
 s1.attach(0); 
 s2.attach(12);
 pinMode(button, INPUT);
 pinMode(button2, INPUT); 
 pinMode(led, OUTPUT); 
 pinMode(motion,INPUT);  
}

void loop() {
  // put your main code here, to run repeatedly:
  Blynk.run();
  //AHT temperature & Humidity 
  if(aht20.available() == true){
    float temperature = aht20.getTemperature(); 
    float humidity = aht20.getHumidity(); 
    Serial.print("Temperature: ");
    Serial.print(temperature, 2);
    Serial.print(" C\t");
    Serial.print("Humidity: ");
    Serial.print(humidity, 2);
    Serial.print("% RH");

    Serial.println();
  }
  
  //Motion sensor
 long state = digitalRead(motion); 
 if(state ==HIGH){
   digitalWrite(led,HIGH); 
   Serial.println("Motion detected"); 
   delay(1000); 
 }
 if(state == LOW){
  digitalWrite(led,LOW); 
  Serial.println("Nothing happen"); 
  delay(1000); 
 }
 //SERVO
 //button 1
 temp = digitalRead(button); 
 
 if (temp == HIGH){
 if(position_target != position_last_set){ 
 s1.write(position_target); 
 s2.write(position_last_set); 
 delay(1000);  
 position_last_set = position_target; 
 } 
 }
if(temp == LOW) {
s2.write(180); 
s1.write(0);  
delay (1000); 

}
//button2 
temp2 = digitalRead(button2); 

}
BLYNK_WRITE(V0){ 
  buttonState = param.asInt(); 
  Serial.print("Button State: "); 
  Serial.println(buttonState); 
  if (buttonState == HIGH){ 
   s1.write(0); 
  s2.write(180);
  }
  else{
   s1.write(180);
   s2.write(0);
  }
}
BLYNK_WRITE(V1){
}
